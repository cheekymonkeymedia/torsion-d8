# Torsion D8
Run `npm install` [possibly `bower install` if bower does not automatically run, from the /UI folder (which is to be placed in [gitroot])]

Compile the theme via [gitroot]/UI. After npm and bower are set up, run 'gulp build', or 'gulp dev'.

Once you have finished theming the first page (home/standard) - take a screenshot and replace screenshot.png with a true representation of the theme.
