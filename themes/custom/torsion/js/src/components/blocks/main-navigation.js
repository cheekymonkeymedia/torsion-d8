/*
 * @file
 * Main navigation, uses intention for DOM manipulation.
 */
 
(function ($, Drupal, window, document) {
  "use strict";

  /**
   * Constructor
   */
  var MainNavigation = function(context, settings) {
    var
      self = this;
    
    self.$elm = $(context).find('#block-mainnavigation');
    self.$loginLink = $(context).find('.login-link');
    
    $('body').once('mainNavigation').each(function() {
      self.init();
    });
  };

  /**
   * @function
   *   Initialize everything.
   */
  MainNavigation.prototype.init = function(context, settings) {
    var
      self = this;

    // Setup intention placements.
    self.$elm.attr('intent', '');
    self.$elm.attr('in-xxlarge-prepend', '.primary-menu .row .columns');
    self.$elm.attr('in-xlarge-prepend', '.primary-menu .row .columns');
    self.$elm.attr('in-large-prepend', '.primary-menu .row .columns');
    self.$elm.attr('in-mediumportrait-append', '.off-canvas .region-primary-menu');
    self.$elm.attr('in-medium-append', '.off-canvas .region-primary-menu');
    self.$elm.attr('in-smallportrait-append', '.off-canvas .region-primary-menu');
    self.$elm.attr('in-small-append', '.off-canvas .region-primary-menu');

    self.$elm.find('li.menu-item--expanded').each(function() {
      $(this).append('<span class="exp-btn"></span>');

      var expBtn = $('.exp-btn');

      expBtn.off('click').on('click', function() {
        $(this).toggleClass('active').siblings('ul.menu').slideToggle('fast');
      });
    });

    /**
     * @function
     *   Remove any orphaned styles / classes left over from foundation's styling of the desktop menu.
     */
    var setupMobileMenu = function() {
      var expBtn = $('.exp-btn');
      
      expBtn.removeClass('active');
      self.$elm.find('> ul.menu').addClass('mobile').find('ul').attr('style', '');
    };

    /**
     * @function
     *   Remove any orphaned styles / classes left over from foundation's styling of the mobile menu.
     *   Close the mobile menu if left open.
     */
    var setupDesktopMenu = function() {
      var expBtn = $('.exp-btn');

      expBtn.removeClass('active');
      if (self.$elm.find('> ul.menu').hasClass('mobile')) {
        $('#offCanvas').foundation('close');
        self.$elm.find('> ul.menu').removeClass('mobile').find('ul').attr('style', '');
      }
    };

    // Setup desktop menu for breakpoint: large.
    IntentContext.intent.on('large', function() {
      setupDesktopMenu();
    });

    // Setup desktop menu for breakpoint: xlarge.
    IntentContext.intent.on('xlarge', function() {
      setupDesktopMenu();
    });

    // Setup desktop menu for breakpoint: xxlarge.
    IntentContext.intent.on('xxlarge', function() {
      setupDesktopMenu();
    });

    // Setup mobile menu for breakpoint: mediumportrait.
    IntentContext.intent.on('mediumportrait', function() {
      setupMobileMenu();
    });

    // Setup mobile menu for breakpoint: medium.
    IntentContext.intent.on('medium', function() {
      setupMobileMenu();
    });

    // Setup mobile menu for breakpoint: smallportrait.
    IntentContext.intent.on('smallportrait', function() {
      setupMobileMenu();
    });

    // Setup mobile menu for breakpoint: small.
    IntentContext.intent.on('small', function() {
      setupMobileMenu();
    });
  };

  /**
   * Instantiate mainNavigation and run it through Drupal attachment.
   */
  Drupal.behaviors.mainNavigation = {
    attach: function (context, settings) {
      var mainNavigation = new MainNavigation(context, settings);
    }
  };
} (jQuery, Drupal, this, this.document));
