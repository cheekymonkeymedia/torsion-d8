/*
 * @file
 * Create placeholder attributes out of the labels on form elements available.
 */
 
(function ($, Drupal, window, document) {
  "use strict";

  /**
   * Constructor
   */
  var FormPlaceholders = function(context, settings) {
    var
      self = this;

    $('body').once('formPlaceholders').each(function() {
      self.init();
    });
  };

  /**
   * @function
   *   Initialize everything.
   */
  FormPlaceholders.prototype.init = function(context, settings) {
    var
      self = this;

    $('form :input[type="text"], form textarea, form :input[type="password"], form :input[type="email"]').each(function(index, elem) {
      var eId = $(elem).attr('id');
      var label = null;
      var required = $(elem).hasClass('required');
      var placeholder = '';
      if (eId && (label = $(elem).parents('form').find('label[for='+eId+']')).length === 1) {
        placeholder = $(label).html();
        $(elem).attr('placeholder', placeholder);
        $(label).addClass('hide-label');
        if (required) {
          $(elem).addClass('placehold-required');
        }
      }
     });
  };

  /**
   * Instantiate mainNavigation and run it through Drupal attachment.
   */
  Drupal.behaviors.formPlaceholders = {
    attach: function (context, settings) {
      var formPlaceholders = new FormPlaceholders(context, settings);
    }
  };
} (jQuery, Drupal, this, this.document));
