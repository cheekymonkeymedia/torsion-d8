/**
 * @file
 * Initilize Foundation.
 */

(function ($, Drupal, window, document) {

  /**
   * Constructor
   */
  var InitFoundation = function(context, settings) {
    var
      self = this;
 
    self.init(context, settings);
  };

  /**
   * @function
   *   Initialize everything.
   */
  InitFoundation.prototype.init = function(context, settings) {
    var
      self = this;

    $(document).foundation();
  };

  /**
   * Instantiate initFoundation and run it through Drupal attachment.
   */
  Drupal.behaviors.initFoundation = {
    attach: function (context, settings) {
      var initFoundation = new InitFoundation(context, settings);
    }
  };
} (jQuery, Drupal, this, this.document));
