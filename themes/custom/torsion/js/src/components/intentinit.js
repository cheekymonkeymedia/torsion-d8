/**
 * @file
 * Initialize intention and run first load DOM manipulation
 */

(function ($, Drupal, window, document) {
  "use strict";

  /**
   * Constructor
   */
  var InitIntent = function(context, settings) {
    var
      self = this;

    $('body').once('initFoundation').each(function() {
      self.init(context, settings);
    });
  };

  /**
   * @function
   *   Initialize intention context.
   */
  InitIntent.prototype.init = function(context, settings) {
    var
      self = this;
    
    // Run intention for first load DOM changes.
    IntentContext.intent.elements(document);
    IntentContext.horizontal_axis.respond();
  };

  /**
   * Instantiate initIntent and run it through Drupal attachment.
   */
  Drupal.behaviors.initIntent = {
    attach: function (context, settings) {
      var initIntent = new InitIntent(context, settings);
    }
  };
} (jQuery, Drupal, this, this.document));
