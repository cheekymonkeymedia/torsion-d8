/**
 * @file
 * Run JS that is used globally on site.
 */

(function ($, Drupal, window, document) {
  "use strict";

  /**
   * Constructor
   */
  var GlobalSite = function(context, settings) {
    var
      self = this;

    $('body').once('globalSite').each(function() {
      self.init(context, settings);
    });
  };

  /**
   * @function
   *   Initialize everything.
   */
  GlobalSite.prototype.init = function(context, settings) {
    var
      self = this;

    $(context).find('body').each(function() {
      // Do stuff
    });
  };

  /**
   * Instantiate globalSite and run it through Drupal attachment.
   */
  Drupal.behaviors.globalSite = {
    attach: function (context, settings) {
      var globalSite = new GlobalSite(context, settings);
    }
  };
}(jQuery, Drupal, this, this.document));
